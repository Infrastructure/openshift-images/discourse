#!/bin/bash
set -x
rm -f ${HOME}/tmp/pids/*.pid

envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
envsubst < /tmp/discourse-configmap/sidekiq.yml > /discourse/config/sidekiq.yml

git config --global --add safe.directory '*'

export RAILS_ENV="production"

bundle exec rake db:migrate || exit 1
bundle exec rake assets:precompile:build || exit 1
SKIP_EMBER_CLI_COMPILE=1 bundle exec rake themes:update || exit 1
SKIP_EMBER_CLI_COMPILE=1 bundle exec rake assets:precompile || exit 1
bundle exec rake maxminddb:get || true

exec env LD_PRELOAD=$RUBY_ALLOCATOR thpoff bundle exec config/unicorn_launcher -E production -c config/unicorn.conf.rb
